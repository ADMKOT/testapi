from flask import Flask, make_response, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from functools import wraps
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import jwt
import uuid


app = Flask(__name__)

app.config['SECRET_KEY'] = 'hardsecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///api.db'

db = SQLAlchemy(app)


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50))
    password = db.Column(db.String(80))
    group_id = db.Column(db.Integer, db.ForeignKey('groups.id'),
        nullable=False)

    def __repr__(self):
        return '<User {}>'.format(self.name)


class Group(db.Model):
    __tablename__ = 'groups'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    users = db.relationship('User', backref='group', lazy='select')
    
    def __repr__(self):
        return '<Group {}>'.format(self.name)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        
        if not token:
            return jsonify({'message' : 'Token is missing!'}), 401

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message' : 'Token is invalid'}), 401
        
        return f(current_user, *args, **kwargs)

    return decorated


@app.route('/user', methods=['GET'])
@token_required
def get_all_users(current_user):
    users = User.query.all()
    output = []
    def filter_user_data(current_user):
        user_data = {}
        user_data['public_id'] = current_user.public_id
        user_data['name'] = current_user.name
        user_data['password'] = current_user.password
        user_data['group'] = current_user.group.name
        output.append(user_data)

        if current_user.group.name == "Users":
            return jsonify({'users' : output })

        elif current_user.group.name == "Moders":
            for user in User.query.filter_by(group_id=3):
                user_data={}
                user_data['public_id'] = user.public_id
                user_data['name'] = user.name
                user_data['password'] = user.password
                user_data['group'] = user.group.name
                output.append(user_data)
            return jsonify({'users' : output })

        elif current_user.group.name == "Admins":
            output_all = []
            for user in users:
                user_data={}
                user_data['public_id'] = user.public_id
                user_data['name'] = user.name
                user_data['password'] = user.password
                user_data['group'] = user.group.name
                output_all.append(user_data)
            return jsonify({'users' : output_all })

    return filter_user_data(current_user)
    

@app.route('/user/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    if not user:
        return jsonify({'message' : 'Not user found!'})

    user_data = {}
    user_data['public_id'] = user.public_id
    user_data['name'] = user.name
    user_data['password'] = user.password

    return jsonify({'user' : user_data})


@app.route('/user', methods=['POST'])
@token_required
def create_user(current_user):
    data = request.get_json()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    hashed_password = generate_password_hash(data['password'], method='sha256')
    new_user = User(public_id=str(uuid.uuid4()), name=data['name'], password=hashed_password,
        group_id='3')
    db.session.add(new_user)
    db.session.commit()

    return jsonify({'message' : 'New user created!'})


@app.route('/user/<public_id>', methods=['PUT'])
@token_required
def promote_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    if not user:
        return jsonify({'message' : 'Not user found!'})

    user.group_id = 2
    db.session.commit()

    return jsonify({'message' : 'The user has been promote!'})


@app.route('/user/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    if not user:
        return jsonify({'message' : 'Not user found!'})

    if current_user.public_id == public_id:
        return jsonify({'message' : 'You cannot delete youself'})
    
    db.session.delete(user)
    db.session.commit()

    return jsonify({'message' : 'The user has been deleted!'})


@app.route('/group', methods=['GET'])
@token_required
def get_all_group(current_user):
    groups = Group.query.all()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    output = []
    for group in groups:
        group_data = {}
        group_data['group_id'] = group.id
        group_data['name'] = group.name
        output.append(group_data)

    return jsonify({'groups' : output})


@app.route('/group', methods=['POST'])
@token_required
def create_group(current_user):
    data = request.get_json()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    new_group = Group(name=data['name'])
    db.session.add(new_group)
    db.session.commit()

    return jsonify({'message' : 'New group created!'})


@app.route('/group/<id>', methods=['DELETE'])
@token_required
def delete_group(current_user, id):
    group = Group.query.filter_by(id=id).first()

    if current_user.group.name != "Admins":
        return jsonify({'message' : 'You are not admin!'})

    if not group:
        return jsonify({'message' : 'Not group found!'})
    
    db.session.delete(group)
    db.session.commit()

    return jsonify({'message' : 'The group has been deleted!'})


@app.route('/login')
def login():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

    user = User.query.filter_by(name=auth.username).first()

    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    
    if check_password_hash(user.password, auth.password):
        token = jwt.encode({'public_id' :  user.public_id,
                            'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30),
                            },
                            app.config['SECRET_KEY'])
        return jsonify({'token' : token.decode('UTF-8')})
    
    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})


if __name__ == '__main__':
    app.run(debug=True)